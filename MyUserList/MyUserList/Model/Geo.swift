//
//  Geo.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation
import SwiftyJSON

class Geo {
    
    var lat: Double?
    var lng: Double?
    
    /*class func fromJSON(_ dict: [String: AnyObject]) -> Geo {
        let geo = Geo()
        geo.lat = dict["lat"] as? Double
        geo.lng = dict["lng"] as? Double
        return geo
    }*/
    
    class func fromJSON(_ json: JSON) -> Geo {
        let geo = Geo()
        geo.lat = json["lat"].doubleValue
        geo.lng = json["lng"].doubleValue
        return geo
    }
}
