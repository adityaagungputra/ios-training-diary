//
//  Company.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation
import SwiftyJSON

class Company {
    
    var name: String?
    var catchPhrase: String?
    var bs: String?
    
    /*class func fromJSON(_ dict: [String: AnyObject]) -> Company {
        let company = Company()
        company.name = dict["name"] as? String
        company.catchPhrase = dict["catchPhrase"] as? String
        company.bs = dict["bs"] as? String
        return company
    }*/
    
    class func fromJSON(_ json: JSON) -> Company {
        let company = Company()
        company.name = json["name"].stringValue
        company.catchPhrase = json["catchPhrase"].stringValue
        company.bs = json["bs"].stringValue
        return company
    }
}
