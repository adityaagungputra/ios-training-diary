//
//  Address.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation
import SwiftyJSON

class Address {
    
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geo: Geo?
    
    /*class func fromJSON(_ dict: [String: AnyObject]) -> Address {
        let address = Address()
        address.street = dict["street"] as? String
        address.suite = dict["suite"] as? String
        address.city = dict["city"] as? String
        address.zipcode = dict["zipcode"] as? String
        address.geo = Geo.fromJSON(dict["geo"] as! [String : AnyObject])
        return address
    }*/
    
    class func fromJSON(_ json: JSON) -> Address {
        let address = Address()
        address.street = json["street"].stringValue
        address.suite = json["suite"].stringValue
        address.city = json["city"].stringValue
        address.zipcode = json["zipcode"].stringValue
        address.geo = Geo.fromJSON(json["geo"])
        return address
    }
}
