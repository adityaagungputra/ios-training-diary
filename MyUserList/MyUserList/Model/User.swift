//
//  User.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
    var address: Address?
    var phone: String?
    var website: String?
    var company: Company?
    
    /*class func fromJSON(_ dict: [String: AnyObject]) -> User {
        let user = User()
        user.id = dict["id"] as? Int
        user.name = dict["name"] as? String
        user.username = dict["username"] as? String
        user.address = Address.fromJSON(dict["address"] as! [String : AnyObject])
        user.email = dict["email"] as? String
        user.phone = dict["phone"] as? String
        user.website = dict["website"] as? String
        user.company = Company.fromJSON(dict["company"] as! [String : AnyObject])
        return user
    }*/
    
    class func fromJSON(_ json: JSON) -> User {
        let user = User()
        user.id = json["id"].intValue
        user.name = json["name"].stringValue
        user.username = json["username"].stringValue
        user.address = Address.fromJSON(json["address"])
        user.email = json["email"].stringValue
        user.phone = json["phone"].stringValue
        user.website = json["website"].stringValue
        user.company = Company.fromJSON(json["company"])
        return user
    }
}
