//
//  MainViewController.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MainViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.dataSource = self
        let nib = UINib(nibName: "UserTableViewCell", bundle: nil)
        table.register(nib, forCellReuseIdentifier: "UserCell")
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 90.0
        table.separatorStyle = .none
        loadUsers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func loadUsers(){
        //Conventional way
        /*let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "https://jsonplaceholder.typicode.com/users/")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            print((response as! HTTPURLResponse).statusCode)
            if error != nil {
                print(error!.localizedDescription)
            } else {
                DispatchQueue.main.async {
                    self.parseUsers(data)
                }
            }
        })
        task.resume()*/
        
        //Alamofire + SwiftyJSON way
        Alamofire.request("https://jsonplaceholder.typicode.com/users/").responseJSON{ response in
            print(response.response!.statusCode)
            if let result = response.result.value {
                //print(result) // this is already a serialized JSON
                let json = JSON(result)
                for obj in json.arrayValue {
                    self.users.append(User.fromJSON(obj))
                }
                self.table.reloadData()
            }
        }
        
        //doPost()
    }
    
    private func parseUsers(_ data: Data?){
        //Native way
        /*do {
            if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [Any] {
                for obj in json {
                    let user = User.fromJSON(obj as! [String : AnyObject])
                    self.users.append(user)
                }
                self.table.reloadData()
            }
        } catch {
            print("Error in JSONSerialization")
        }*/
        
        let swiftyJsonVar = JSON(data: data!)
        for obj in swiftyJsonVar.arrayValue {
            self.users.append(User.fromJSON(obj))
        }
        self.table.reloadData()
    }
    
    private func doPost(){
        //Native way
        /*var request = URLRequest(url: URL(string: "https://jsonplaceholder.typicode.com/posts")!)
        request.httpMethod = "POST"
        let params = ["title": "My Post",
                      "body": "This is my new post",
                      "userId": 1] as [String: Any]
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            print("Response code: \((response as! HTTPURLResponse).statusCode)")
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: AnyObject] {
                    print(json["title"]!)
                    print(json["body"]!)
                }
            } catch {
                print("Error in JSONSerialization")
            }
        })
        task.resume()*/
        
        let params: Parameters = ["title": "My Post",
                                  "body": "This is my new post",
                                  "userId": 1]
        
        Alamofire.request("https://jsonplaceholder.typicode.com/posts", method: .post, parameters: params, encoding: URLEncoding.httpBody)
            .responseJSON {response in
                print(response.response!.statusCode)
                if let result = response.result.value {
                    let json = JSON(result)
                    print(json["title"])
                    print(json["body"])
                }
        }
    }
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserTableViewCell
        cell.user = users[indexPath.row]
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1.0
        cell.layer.cornerRadius = 3.0
        return cell
    }
}
