//
//  UserTableViewCell.swift
//  MyUserList
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    var user: User! {
        didSet {
            if let user = user {
                nameLabel.text = user.name
                emailLabel.text = user.email
                companyLabel.text = user.company?.name
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
