//
//  HomeViewController.swift
//  MyFirstApp
//
//  Created by MAC-05 on 4/19/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var txtField: UITextField!
    
    @IBOutlet weak var btnDialog: UIButton!
    
    var widgetsViewController: WidgetsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        widgetsViewController = self.storyboard?.instantiateViewController(withIdentifier: "widgetsVC") as! WidgetsViewController
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Listeners
    @IBAction func onTapGestureRecognized(_ sender: UITapGestureRecognizer) {
        txtField.resignFirstResponder()
    }
    
    @IBAction func onFinishEditing(_ sender: UITextField) {
        txtField.resignFirstResponder()
    }
    
    @IBAction func onTextChanged(_ sender: UITextField) {
        btnDialog.isEnabled = !txtField.text!.isEmpty
    }
    
    @IBAction func onMoreControlsClicked(_ sender: UIButton) {
        widgetsViewController.nameText = txtField.text!
        show(widgetsViewController, sender: nil)
    }
    
    @IBAction func onTryDialog(_ sender: UIButton) {
        let controller = UIAlertController(title: "Wanna show the dialog?", message: "This is the action message", preferredStyle: .actionSheet)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: {action in self.onYesClicked()})
        let noAction = UIAlertAction(title: "Ah, okay!", style: .cancel, handler: {action in self.onNoClicked()})
        
        controller.addAction(yesAction)
        controller.addAction(noAction)
        
        if let ppc = controller.popoverPresentationController {
            ppc.sourceView = sender
            ppc.sourceRect = sender.bounds
            ppc.permittedArrowDirections = .down
        }
        
        present(controller, animated: true, completion: nil)
    }
    
    // MARK: - Private Methods
    private func onYesClicked(){
        let msg = txtField.text!.isEmpty ? "Ah, text field is empty" : "Ah, there is \(txtField.text!)"
        let controller2 = UIAlertController(title: "Dialog showed", message: msg, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        controller2.addAction(cancelAction)
        self.present(controller2, animated: true, completion: nil)
    }
    
    private func onNoClicked(){
        print("Dialog won't appear")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
