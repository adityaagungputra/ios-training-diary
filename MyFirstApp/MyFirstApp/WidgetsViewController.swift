//
//  WidgetsViewController.swift
//  MyFirstApp
//
//  Created by MAC-05 on 4/19/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class WidgetsViewController: UIViewController {

    @IBOutlet weak var lblRemark: UILabel!
    
    var nameText: String? {
        didSet {
            configurateNavbarTitle()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configurateNavbarTitle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onDateChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM yyyy"
        lblRemark.text = "Selected date: \(dateFormatter.string(from: sender.date))"
    }

    @IBAction func onSliderValueChanged(_ sender: UISlider) {
        lblRemark.text = "Slider value now: \(sender.value)"
    }
    
    @IBAction func onSwitchChanged(_ sender: UISwitch) {
        lblRemark.text = "Switch status now: \(sender.isOn)"
    }
    
    @IBAction func onSegmentedControlChanged(_ sender: UISegmentedControl) {
        lblRemark.text = "Segmented control selected: \(sender.titleForSegment(at: sender.selectedSegmentIndex)!)"
    }
    
    fileprivate func configurateNavbarTitle(){
        if let titleText = nameText {
            self.title = "\(titleText)'s Home"
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
