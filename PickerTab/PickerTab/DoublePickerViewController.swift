//
//  DoublePickerViewController.swift
//  PickerTab
//
//  Created by MAC-05 on 5/1/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class DoublePickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var doublePicker: UIPickerView!
    
    private let charComponent = 0
    private let countryComponent = 1
    private let charNames = ["Nobita", "Shizuka", "Dekisugi", "Giant", "Jaiko", "Suneo", "Dorami", "Doraemon", "Nobisuke"]
    private let countryNames = ["Japan", "Korea", "China", "Russia", "Singapore", "India", "Australia"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        doublePicker.dataSource = self
        doublePicker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onButtonPressed(_ sender: UIButton) {
        let charRow = doublePicker.selectedRow(inComponent: charComponent)
        let countryRow = doublePicker.selectedRow(inComponent: countryComponent)
        
        let selectedChar = charNames[charRow]
        let selectedCountry = countryNames[countryRow]
        let title = "You selected \(selectedChar) and \(selectedCountry)!"
        
        let alert = UIAlertController(title: title, message: "Thanks", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Picker Data Source Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == charComponent {
            return charNames.count
        } else {
            return countryNames.count
        }
    }
    
    // MARK: - Picker Delegate Methods
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == charComponent {
            return charNames[row]
        } else {
            return countryNames[row]
        }
    }
}
