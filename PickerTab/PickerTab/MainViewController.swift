//
//  MainViewController.swift
//  PickerTab
//
//  Created by MAC-05 on 5/7/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    private var singlePickerVC: SinglePickerViewController!
    private var doublePickerVC: DoublePickerViewController!
    private var customPickerVC: CustomPickerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        singlePickerVC = SinglePickerViewController()
        doublePickerVC = DoublePickerViewController()
        customPickerVC = CustomPickerViewController()
        
        self.viewControllers = [singlePickerVC, doublePickerVC, customPickerVC]
        singlePickerVC.tabBarItem = UITabBarItem(title: "Single", image: UIImage(named: "ic_build"), tag: 0)
        doublePickerVC.tabBarItem = UITabBarItem(title: "Double", image: UIImage(named: "ic_build"), tag: 1)
        customPickerVC.tabBarItem = UITabBarItem(title: "Custom", image: UIImage(named: "ic_build"), tag: 2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
