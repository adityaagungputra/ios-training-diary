//
//  ModalViewController.swift
//  MySegue
//
//  Created by MAC-05 on 4/26/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    
    var labelText: String? {
        didSet{
            configurateView()
        }
    }
    var delegate: MainViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configurateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func configurateView(){
        if let sentString = labelText {
            if let label = myLabel {
                label.text = sentString
            }
        } else {
            myLabel.text = "-"
        }
    }

    @IBAction func onCancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        if let delegate = delegate {
            delegate.onVCDismissed(dismissedController: self, text: "ModalVC dismissed")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

protocol ModalVCDelegate {
    func onVCDismissed(dismissedController: ModalViewController, text: String)
}
