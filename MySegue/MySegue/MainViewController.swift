//
//  MainViewController.swift
//  MySegue
//
//  Created by MAC-05 on 4/25/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var myTextField: UITextField!
    var showViewController: UIViewController!
    var modalVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        showViewController = self.storyboard?.instantiateViewController(withIdentifier: "showVC") as! ShowViewController
        modalVC = self.storyboard?.instantiateViewController(withIdentifier: "modalVC") as! ModalViewController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "modalSegue" {
            let controller = segue.destination as! ModalViewController
            controller.labelText = myTextField.text!
        }
    }
    
    @IBAction func unwind(unwindSegue: UIStoryboardSegue) {
        if let sourceViewController = unwindSegue.source as? ModalViewController {
            if let labelText = sourceViewController.labelText {
                print("Transferred text: \(labelText)")
            } else {
                print("No text transferred")
            }
        }
    }
    
    @IBAction func onShowClicked(_ sender: Any) {
        show(showViewController, sender: nil)
    }

    @IBAction func onModalWithCode(_ sender: UIButton) {
        let modal = modalVC as! ModalViewController
        modal.labelText = myTextField.text!
        modal.delegate = self
        present(modal, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension MainViewController: ModalVCDelegate {
    func onVCDismissed(dismissedController: ModalViewController, text: String){
        print(text)
    }
}
