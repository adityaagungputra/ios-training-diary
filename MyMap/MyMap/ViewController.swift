//
//  ViewController.swift
//  MyMap
//
//  Created by MAC-05 on 6/11/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10.0
        
        //auto authorization
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        }
        
        mapView.showsUserLocation = true
    }
    
    @IBAction func onZoom(_ sender: UIBarButtonItem) {
        let locValue = locationManager.location!.coordinate
        print("Locations: \(locValue.latitude) : \(locValue.longitude)")
        
        let userLoc = mapView.userLocation
        if let location = userLoc.location {
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
            mapView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func onType(_ sender: UIBarButtonItem) {
        if mapView.mapType == .standard {
            mapView.mapType = .satellite
        } else {
            mapView.mapType = .standard
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            locationManager.stopUpdatingLocation()
        }
    }
    
    private func locationManager(_ manager: CLLocationManager, didFailWithError error: NSError) {
        let errorType = (error.code == CLError.denied.rawValue) ? "Access denied" : "Error \(error.code)"
        let alert = UIAlertController(title: "Location manager error", message: errorType, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("Locations from manager: \(locValue.latitude) \(locValue.longitude)")
        
        let annotation = MKPointAnnotation()
        let coordinate = mapView.userLocation.coordinate
        annotation.coordinate = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude)
        mapView.addAnnotation(annotation)
    }
}

