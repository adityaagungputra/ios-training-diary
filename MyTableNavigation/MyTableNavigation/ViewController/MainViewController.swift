//
//  MainViewController.swift
//  MyTableNavigation
//
//  Created by MAC-05 on 6/4/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {

    @IBOutlet var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let simpleTableVC = SimpleTableViewController()
            self.navigationController?.pushViewController(simpleTableVC, animated: true)
        } else if indexPath.row == 1 {
            let collectionVC = CollectionViewController()
            self.navigationController?.pushViewController(collectionVC, animated: true)
        }
    }
}
