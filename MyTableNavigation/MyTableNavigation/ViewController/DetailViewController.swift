//
//  DetailViewController.swift
//  MyTableNavigation
//
//  Created by MAC-05 on 6/4/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var countryName: UILabel!
    var country: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Country Detail"
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initView(){
        if let country = country {
            countryName.text = country
        }
    }
}
