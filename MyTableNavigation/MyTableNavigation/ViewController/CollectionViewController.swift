//
//  CollectionViewController.swift
//  MyTableNavigation
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var countries = [
        "India", "Nepal", "Vietnam", "China", "Japan", "Korea", "Russia",
        "Norway", "Italy", "Spain", "Canada", "Brazil", "Peru",
        "France", "Germany", "Greece", "Iran", "Iraq", "Taiwan", "Thailand", "Iceland",
        "Mexico", "Indonesia", "Egypt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName:"CountryViewCell", bundle: nil), forCellWithReuseIdentifier: "CellId")
        title = "Collection View"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath) as! CountryViewCell
        cell.country = countries[indexPath.row]
        cell.bind()
        cell.backgroundColor = UIColor.blue
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(countries[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let nColumn = 3
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(nColumn - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(nColumn))
        return CGSize(width: size, height: size)
    }
}
