//
//  SimpleTableViewController.swift
//  MyTableNavigation
//
//  Created by MAC-05 on 6/4/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class SimpleTableViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let tableId = "tableId"
    var countries = [
        "India", "Nepal", "Vietnam", "China", "Japan", "Korea", "Russia",
        "Norway", "Italy", "Spain", "Canada", "Brazil", "Peru",
        "France", "Germany", "Greece", "Iran", "Iraq", "Taiwan", "Thailand", "Iceland",
        "Mexico", "Indonesia", "Egypt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.delegate = self
        table.dataSource = self
        title = "Countries Table"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SimpleTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
	
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = table.dequeueReusableCell(withIdentifier: tableId)
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: tableId)
        }
        cell?.textLabel?.text = countries[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = countries[indexPath.row]
        let detailVC = DetailViewController()
        detailVC.country = country
        self.navigationController?.pushViewController(detailVC, animated: true)
        table.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            countries.remove(at: indexPath.row)
            table.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
