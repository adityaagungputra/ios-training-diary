//
//  CountryViewCell.swift
//  MyTableNavigation
//
//  Created by MAC-05 on 6/5/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class CountryViewCell: UICollectionViewCell {

    @IBOutlet weak var name: UILabel!
    
    var country: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(){
        if let country = country {
            name.text = country
        }
    }
}
