//
//  Constants.swift
//  MySettings
//
//  Created by MAC-05 on 6/28/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation

let officerKey = "officer"
let authKey = "authorizationCode"
let rankKey = "rank"
let warpDriveKey = "warp"
let warpFactorKey = "warpFactor"
