//
//  DisplayViewController.swift
//  MySettings
//
//  Created by MAC-05 on 6/28/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController {

    @IBOutlet weak var officerLabel: UILabel!
    @IBOutlet weak var authLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var warpDriveLabel: UILabel!
    @IBOutlet weak var warpFactorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let app = UIApplication.shared
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillEnterForeground(notification:)), name: Notification.Name.UIApplicationWillEnterForeground, object: app)
        initFields()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func applicationWillEnterForeground(notification: NSNotification){
        let setting = UserDefaults.standard
        setting.synchronize()
        initFields()
    }
    
    private func initFields(){
        let setting = UserDefaults.standard
        officerLabel.text = setting.string(forKey: officerKey)
        authLabel.text = setting.string(forKey: authKey)
        rankLabel.text = setting.string(forKey: rankKey)
        warpDriveLabel.text = setting.bool(forKey: warpDriveKey) ? "Engaged" : "Disabled"
        warpFactorLabel.text = (setting.object(forKey: warpFactorKey) as AnyObject).stringValue
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
