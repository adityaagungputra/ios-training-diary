//
//  SettingViewController.swift
//  MySettings
//
//  Created by MAC-05 on 6/28/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var engineSwitch: UISwitch!
    @IBOutlet weak var warpSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let app = UIApplication.shared
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationWillEnterForeground(notification:)), name: Notification.Name.UIApplicationWillEnterForeground, object: app)
        initFields()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func applicationWillEnterForeground(notification: NSNotification){
        let setting = UserDefaults.standard
        setting.synchronize()
        initFields()
    }
    
    private func initFields(){
        let setting = UserDefaults.standard
        engineSwitch.isOn = setting.bool(forKey: warpDriveKey)
        warpSlider.value = setting.float(forKey: warpFactorKey)
    }
    
    @IBAction func onEngineSwitched(_ sender: UISwitch) {
        let setting = UserDefaults.standard
        setting.set(engineSwitch.isOn, forKey: warpDriveKey)
        setting.synchronize()
    }
    
    @IBAction func onWarpSlided(_ sender: UISlider) {
        let setting = UserDefaults.standard
        setting.set(warpSlider.value, forKey: warpFactorKey)
        setting.synchronize()
    }
    
    @IBAction func onOpenAppSetting(_ sender: UIButton) {
        let application = UIApplication.shared
        let url = URL(string: UIApplicationOpenSettingsURLString)! as URL
        if application.canOpenURL(url){
            application.open(url, options: ["":""], completionHandler: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
