//
//  MainViewController.swift
//  MySettings
//
//  Created by MAC-05 on 6/28/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    private var displayVC: DisplayViewController!
    private var settingVC: SettingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        displayVC = DisplayViewController()
        settingVC = SettingViewController()
        
        self.viewControllers = [displayVC, settingVC]
        displayVC.tabBarItem = UITabBarItem(title: "Display", image: UIImage(named: "ic_chat"), tag: 0)
        settingVC.tabBarItem = UITabBarItem(title: "Setting", image: UIImage(named: "ic_help"), tag: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
