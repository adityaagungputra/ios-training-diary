//
//  MainViewController.swift
//  MultiviewApp
//
//  Created by MAC-05 on 4/27/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var mainView: UIView!
    
    var blueViewController: BlueViewController!
    var yellowViewController: YellowViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var firstVC: UIViewController
        if segControl.selectedSegmentIndex == 0 {
            blueViewController = BlueViewController()
            firstVC = blueViewController
        } else {
            yellowViewController = YellowViewController()
            firstVC = yellowViewController
        }
        firstVC.view.frame = mainView.bounds
        switchView(from: nil, to: firstVC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        if blueViewController != nil && blueViewController!.view.superview == nil {
            blueViewController = nil
        }
        if yellowViewController != nil && yellowViewController!.view.superview == nil {
            yellowViewController = nil
        }
    }
    
    @IBAction func onControlChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if blueViewController?.view.superview == nil {
                if blueViewController == nil {
                    blueViewController = BlueViewController()
                }
            }
            blueViewController.view.frame = mainView.bounds
            switchView(from: yellowViewController, to: blueViewController)
        } else {
            if yellowViewController?.view.superview == nil {
                if yellowViewController == nil {
                    yellowViewController = YellowViewController()
                }
            }
            yellowViewController.view.frame = mainView.bounds
            switchView(from: blueViewController, to: yellowViewController)
        }
    }

    private func switchView(from fromVC: UIViewController?, to toVC: UIViewController?){
        if fromVC != nil {
            //remove view from its parent
            fromVC!.willMove(toParentViewController: nil)
            fromVC!.view.removeFromSuperview()
            fromVC!.removeFromParentViewController()
        }
        
        if toVC != nil {
            //add the child view to 'self'
            addChildViewController(toVC!)
            mainView.addSubview(toVC!.view)
            //mainView.insertSubview(toVC!.view, at: 0)
            toVC!.didMove(toParentViewController: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
