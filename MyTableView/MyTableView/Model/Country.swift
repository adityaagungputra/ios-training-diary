//
//  Country.swift
//  MyTableView
//
//  Created by MAC-05 on 6/4/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import Foundation

class Country {
    
    var countryName: String?
    var capital: String?
    
    init(_ name: String, _ capital: String){
        self.countryName = name
        self.capital = capital
    }
}
