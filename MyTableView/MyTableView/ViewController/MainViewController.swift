//
//  MainViewController.swift
//  MyTableView
//
//  Created by MAC-05 on 6/3/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    private var simpleTableVC: SimpleTableViewController!
    private var customTableVC: CustomTableViewController!
    private var groupedTableVC: GroupedTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        simpleTableVC = SimpleTableViewController()
        customTableVC = CustomTableViewController()
        groupedTableVC = GroupedTableViewController()
        self.viewControllers = [simpleTableVC, customTableVC, groupedTableVC]
        simpleTableVC.tabBarItem = UITabBarItem(title: "Simple", image: nil, tag: 0)
        customTableVC.tabBarItem = UITabBarItem(title: "Custom Cell", image: nil, tag: 1)
        groupedTableVC.tabBarItem = UITabBarItem(title: "Grouped", image: nil, tag: 2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
