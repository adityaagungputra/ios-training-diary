//
//  SearchSimpleViewController.swift
//  MyTableView
//
//  Created by MAC-05 on 6/4/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class SearchSimpleViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let tableId = "tableId"
    var countries: [String] = [] //initial data
    var filteredCountries: [String] = [] //filtered displayed data
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchSimpleViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        if let query = searchController.searchBar.text {
            filteredCountries.removeAll(keepingCapacity: true)
            if !query.isEmpty {
                //filter function for each String
                let filter: (String) -> Bool = { name in
                    let range = name.range(of: query, options: NSString.CompareOptions.caseInsensitive, range: nil, locale: nil)
                    return range != nil
                }
                filteredCountries = countries.filter(filter)
            }
        }
        table.reloadData()
    }
}

extension SearchSimpleViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: tableId)
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: tableId)
        }
        
        let defaultImage = UIImage(named: "ic_face")
        cell?.imageView?.image = defaultImage
        let selectedImage = UIImage(named: "ic_pets")
        cell?.imageView?.highlightedImage = selectedImage
        cell?.textLabel?.text = filteredCountries[indexPath.row]
        return cell!
    }
}
