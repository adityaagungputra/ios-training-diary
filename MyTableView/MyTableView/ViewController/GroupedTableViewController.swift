//
//  GroupedTableViewController.swift
//  MyTableView
//
//  Created by MAC-05 on 6/3/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class GroupedTableViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    let tableId = "CountryCellId"
    var countries: [String: [Country]]!
    var keys: [String]!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.dataSource = self
        let xib = UINib(nibName: "CountryViewCell", bundle: nil)
        table.register(xib, forCellReuseIdentifier: tableId)
        table.rowHeight = 70
        
        countries = [String: [Country]]()
        countries["Asia"] = [Country("Japan", "Tokyo"), Country("South Korea", "Seoul"), Country("India", "New Delhi"),
                             Country("Indonesia", "Jakarta"), Country("China", "Beijing"), Country("Vietnam", "Hanoi"),
                             Country("Thailand", "Bangkok"), Country("Malaysia", "Kuala Lumpur")]
        countries["America"] = [Country("USA", "Washington DC"), Country("Canada", "Ottawa"), Country("Mexico", "Mexico City")]
        countries["Europe"] = [Country("Paris", "France"), Country("Italy", "Rome"), Country("Germany", "Berlin"), Country("England", "London"),
                               Country("Turkey", "Ankara"), Country("Norway", "Oslo")]
        keys = countries.keys.sorted()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GroupedTableViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = keys[section]
        let countrySection = countries[key]!
        return countrySection.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return keys[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableId, for: indexPath) as! CountryViewCell
        let key = keys[indexPath.section]
        let countrySection = countries[key]!
        cell.bind(country: countrySection[indexPath.row])
        return cell
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return keys
    }
}
