//
//  CustomTableViewController.swift
//  MyTableView
//
//  Created by MAC-05 on 6/3/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class CustomTableViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let cellId = "CountryCellId"
    var countries = [Country("Japan", "Tokyo"), Country("China", "Beijing"),
                     Country("Italy", "Rome"), Country("France", "Paris"),
                     Country("England", "London"), Country("Vietnam", "Hanoi"),
                     Country("Egypt", "Kairo"), Country("Indonesia", "Jakarta"),
                     Country("Norway", "Oslo"), Country("Chile", "Santiago"),
                     Country("Thailand", "Bangkok"), Country("Canada", "Ottawa")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        table.delegate = self
        table.dataSource = self
        //table.register(CountryViewCell.self, forCellReuseIdentifier: cellId)
        let xib = UINib(nibName: "CountryViewCell", bundle: nil)
        table.register(xib, forCellReuseIdentifier: cellId)
        table.rowHeight = 80
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CustomTableViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CountryViewCell
        let country = countries[indexPath.row]
        cell.bind(country: country)
        return cell
    }
}
