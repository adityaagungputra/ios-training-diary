//
//  SimpleTableViewController.swift
//  MyTableView
//
//  Created by MAC-05 on 6/3/17.
//  Copyright © 2017 MAC-05. All rights reserved.
//

import UIKit

class SimpleTableViewController: UIViewController {

    @IBOutlet weak var simpleTable: UITableView!
    var searchController: UISearchController!
    
    let tableId = "tableId"
    var countries = [
        "India", "Nepal", "Vietnam", "China", "Japan", "Korea", "Russia",
        "Norway", "Italy", "Spain", "Canada", "Brazil", "Peru",
        "France", "Germany", "Greece", "Iran", "Iraq", "Taiwan", "Thailand", "Iceland",
        "Mexico", "Indonesia", "Egypt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let resultController = SearchSimpleViewController()
        resultController.countries = countries
        searchController = UISearchController(searchResultsController: resultController)
        let searchBar = searchController.searchBar
        searchBar.placeholder = "Type a country name"
        searchBar.sizeToFit()
        simpleTable.tableHeaderView = searchBar
        searchController.searchResultsUpdater = resultController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SimpleTableViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = simpleTable.dequeueReusableCell(withIdentifier: tableId)
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: tableId)
        }
        let defaultImage = UIImage(named: "ic_face")
        cell?.imageView?.image = defaultImage
        let selectedImage = UIImage(named: "ic_pets")
        cell?.imageView?.highlightedImage = selectedImage
        cell?.textLabel?.text = countries[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowValue = countries[indexPath.row]
        let message = "You selected \(rowValue)"
        let alert = UIAlertController(title: "Row Selected", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {action in
            self.simpleTable.deselectRow(at: indexPath, animated: true)
        })
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
